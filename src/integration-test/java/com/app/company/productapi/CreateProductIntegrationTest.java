package com.app.company.productapi;

import com.app.company.productapi.dto.CreateProductRequest;
import com.app.company.productapi.entities.ProductEntity;
import com.app.company.productapi.repository.ProductRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("intTest")
@SpringBootTest
@AutoConfigureMockMvc
public class CreateProductIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ProductRepository productRepository;

    @Test
    void createNewProduct() throws Exception {
        CreateProductRequest createProductRequest = CreateProductRequest.builder()
                .productName("Cristal")
                .productType("Cerveza")
                .statisticFlag(true)
                .alcohol(true)
                .isActive(true)
                .details(CreateProductRequest.Details.builder()
                        .productBrand("Cristal")
                        .productCaliber("1L")
                        .productFlavor("Normal")
                        .productFamily("Normal")
                        .recyclable(true).build())
                .build();

        String requestJson = new ObjectMapper().writeValueAsString(createProductRequest);

        mockMvc.perform(post("/api/v1/products")
                        .contentType("application/json")
                        .param("sendWelcomeMail", "true")
                        .content(requestJson))
                .andExpect(status().isCreated());

        final Optional<ProductEntity> productEntity = productRepository.findById(1l);

        assertNotNull(productEntity.get());
        assertEquals("Cristal", productEntity.get().getName());
    }
}

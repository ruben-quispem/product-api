package com.app.company.productapi.resources;

import com.app.company.productapi.dto.CreateProductRequest;
import com.app.company.productapi.dto.GetProductDetailsResponse;
import com.app.company.productapi.dto.GetProductResponse;
import com.app.company.productapi.dto.UpdateProductRequest;
import com.app.company.productapi.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("api/v1/products")
@Slf4j
public class ProductApi {

    private final ProductService productService;

    @Autowired
    public ProductApi(ProductService productService) {
        this.productService = productService;
    }

    @Operation(summary = "Creates a new product")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Product created",
                    content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = CreateProductRequest.class))})})
    @PostMapping
    public ResponseEntity<?> saveProduct(@Valid @RequestBody CreateProductRequest createProductRequest) {
        productService.saveProduct(createProductRequest);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Operation(summary = "Updates information for an existing product")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Product updated",
                    content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = UpdateProductRequest.class))})})
    @PutMapping("/{id}")
    public ResponseEntity<?> updateProduct(@PathVariable Long id, @RequestBody UpdateProductRequest updateProductRequest) {
        productService.updateProduct(id, updateProductRequest);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @Operation(summary = "Retrieves product information by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Retrieves the product info",
                    content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = GetProductResponse.class))}),
            @ApiResponse(responseCode = "404", description = "Product not found for provided id",
                    content = @Content)})
    @GetMapping("/{id}")
    public ResponseEntity<GetProductResponse> retrieveProductById(@PathVariable Long id) {
        return ResponseEntity.ok().body(productService.retrieveProductById(id));
    }


    @Operation(summary = "Retrieves product details information by ID")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Retrieves the product details info",
                    content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = GetProductDetailsResponse.class))}),
            @ApiResponse(responseCode = "404", description = "Product details not found for provided id",
                    content = @Content)})
    @GetMapping("/{id}/details")
    public ResponseEntity<GetProductDetailsResponse> retrieveProductDetailsById(@PathVariable Long id) {
        return ResponseEntity.ok().body(productService.retrieveProductDetailsById(id));
    }
}

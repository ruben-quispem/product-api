package com.app.company.productapi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GetProductDetailResponse {
    @NotEmpty(message = "Product brand is mandatory")
    private String productBrand;
    @NotEmpty(message = "Product caliber is mandatory")
    private String productCaliber;
    @NotEmpty(message = "Product family is mandatory")
    private String productFamily;
    @NotEmpty(message = "Product flavor is mandatory")
    private String productFlavor;
    private boolean recyclable;
}

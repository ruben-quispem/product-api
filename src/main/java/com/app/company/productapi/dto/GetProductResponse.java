package com.app.company.productapi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.time.LocalDateTime;

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class GetProductResponse {

    @NotEmpty(message = "Company is mandatory")
    private String company;
    @NotEmpty(message = "Address is mandatory")
    private String address;
    @NotEmpty(message = "Product name is mandatory")
    private String productName;
    @NotEmpty(message = "Product type is mandatory")
    private String productType;
    private boolean statisticFlag;
    private boolean alcohol;
    private boolean isActive;

    @Min(0)
    private int stockQuantity;
    private double saleAverage;
    private String avatar;
    private LocalDateTime createdAt;
}

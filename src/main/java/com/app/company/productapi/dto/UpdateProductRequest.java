package com.app.company.productapi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class UpdateProductRequest {

    private String productName;
    private String productType;
    private boolean statisticFlag;
    private boolean alcohol;
    private boolean isActive;
    private UpdateProductRequest.Details details;


    @Builder
    @Getter
    @Setter
    @ToString
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Details {
        private String productBrand;
        private String productCaliber;
        private String productFamily;
        private String productFlavor;
        private boolean recyclable;
    }
}

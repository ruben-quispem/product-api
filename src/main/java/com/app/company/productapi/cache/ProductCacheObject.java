package com.app.company.productapi.cache;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ProductCacheObject {
    private String company;
    private String address;
}

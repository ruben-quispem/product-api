package com.app.company.productapi.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@Component
public class InMemoryProductCacheProvider implements ProductCache {

    private CacheLoader<String, String> loader;
    private LoadingCache<String, String> cache;

    @PostConstruct
    public void init() {
        preloadProductCache();
    }

    @Override
    public ProductCacheObject getProductCache() throws ExecutionException {
        return ProductCacheObject.builder()
                .company(cache.get("company"))
                .address(cache.get("address"))
                .build();
    }

    private void preloadProductCache() {
        loader = new CacheLoader<>() {
            @Override
            public String load(String key) {
                return key.toUpperCase();
            }
        };

        cache = CacheBuilder.newBuilder().build(loader);
        Map<String, String> map = new HashMap<>();
        map.put("company", "Product ABC");
        map.put("address", "2129 Old Dear Lane");
        cache.putAll(map);
    }
}

package com.app.company.productapi.cache;

import java.util.concurrent.ExecutionException;

public interface ProductCache {

    ProductCacheObject getProductCache() throws ExecutionException;
}

package com.app.company.productapi.entities;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Builder(toBuilder = true)
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
@Getter
@Table(name = "GROUPED_PRODUCT")
@ToString(exclude = {"productEntity"})
public class GroupedProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;
    @Column(name = "brand_name")
    private String brandName;
    @Column(name = "caliber_name")
    private String caliberName;
    @Column(name = "family_name")
    private String familyName;
    @Column(name = "flavor_name")
    private String flavorName;
    @Column(name = "is_recyclable")
    private boolean recyclable;
    @OneToOne(mappedBy = "groupedProductsEntity")
    @JoinColumn(name = "productId")
    private ProductEntity productEntity;
}

package com.app.company.productapi.mapper;

import com.app.company.productapi.dto.CreateProductRequest;
import com.app.company.productapi.dto.GetProductDetailsResponse;
import com.app.company.productapi.entities.GroupedProductEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface GroupedProductMapper {

    @Mappings({
            @Mapping(target = "brandName", source = "details.productBrand"),
            @Mapping(target = "caliberName", source = "details.productCaliber"),
            @Mapping(target = "familyName", source = "details.productFamily"),
            @Mapping(target = "flavorName", source = "details.productFlavor"),
            @Mapping(target = "recyclable", source = "details.recyclable"),
    })
    GroupedProductEntity createProductDetailsRequestToGroupedProductEntity(CreateProductRequest.Details details);
    @Mappings({
            @Mapping(source = "brandName", target = "productBrand"),
            @Mapping(source = "caliberName", target = "productCaliber"),
            @Mapping(source = "familyName", target = "productFamily"),
            @Mapping(source = "flavorName", target = "productFlavor"),
            @Mapping(source = "recyclable", target = "recyclable"),
    })
    GetProductDetailsResponse createGroupedProductEntityToGetProductDetailsResponse(GroupedProductEntity groupedProductEntity);
}

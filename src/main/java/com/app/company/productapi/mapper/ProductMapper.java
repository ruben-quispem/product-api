package com.app.company.productapi.mapper;

import com.app.company.productapi.dto.CreateProductRequest;
import com.app.company.productapi.dto.GetProductResponse;
import com.app.company.productapi.dto.UpdateProductRequest;
import com.app.company.productapi.entities.ProductEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {GroupedProductMapper.class})
public interface ProductMapper {

    @Mappings({
        @Mapping(target = "name", source = "productName"),
        @Mapping(target = "type", source = "productType"),
        @Mapping(target = "groupedProductsEntity", source = "details")
    })
    ProductEntity createProductRequestToProductEntity(CreateProductRequest createProductRequest);

    @Mappings({
            @Mapping(target = "productName", source = "name"),
            @Mapping(target = "productType", source = "type"),
    })
    GetProductResponse productEntityToGetProductResponse(ProductEntity productEntity);

    @Mappings({
            @Mapping(target = "name", source = "productName"),
            @Mapping(target = "type", source = "productType"),
            @Mapping(target = "groupedProductsEntity", source = "details")
    })
    void updateProductEntityWithUpdateProductRequest(@MappingTarget ProductEntity productEntity,
                                                     UpdateProductRequest updateProductRequest);
}

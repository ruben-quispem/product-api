package com.app.company.productapi.service;

import com.app.company.productapi.dto.CreateProductRequest;
import com.app.company.productapi.dto.GetProductDetailsResponse;
import com.app.company.productapi.dto.GetProductResponse;
import com.app.company.productapi.dto.UpdateProductRequest;

public interface ProductService {
    GetProductResponse retrieveProductById(long id);
    void saveProduct(CreateProductRequest productRequest);
    GetProductDetailsResponse retrieveProductDetailsById(long id);
    void updateProduct(Long id, UpdateProductRequest updateProductRequest);
}

package com.app.company.productapi.service;

import com.app.company.productapi.cache.ProductCache;
import com.app.company.productapi.cache.ProductCacheObject;
import com.app.company.productapi.dto.CreateProductRequest;
import com.app.company.productapi.dto.GetProductDetailsResponse;
import com.app.company.productapi.dto.GetProductResponse;
import com.app.company.productapi.dto.UpdateProductRequest;
import com.app.company.productapi.entities.ProductEntity;
import com.app.company.productapi.exceptions.ProductCacheException;
import com.app.company.productapi.exceptions.ProductNotFoundException;
import com.app.company.productapi.integration.MasterProductIntegration;
import com.app.company.productapi.integration.data.MasterProduct;
import com.app.company.productapi.mapper.GroupedProductMapper;
import com.app.company.productapi.mapper.ProductMapper;
import com.app.company.productapi.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;

@Service
@Slf4j
public class DefaultProductServiceProvider implements ProductService{

    private final MasterProductIntegration productIntegration;
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;
    private final GroupedProductMapper groupedProductMapper;
    private final ProductCache productCache;

    @Autowired
    public DefaultProductServiceProvider(MasterProductIntegration productIntegration,
                                         ProductRepository productRepository,
                                         ProductMapper productMapper,
                                         GroupedProductMapper groupedProductMapper, ProductCache productCache) {
        this.productIntegration = productIntegration;
        this.productRepository = productRepository;
        this.productMapper = productMapper;
        this.groupedProductMapper = groupedProductMapper;
        this.productCache = productCache;
    }

    @Override
    public GetProductResponse retrieveProductById(long id) {
        return createGetProductResponse(id);
    }

    @Override
    public void saveProduct(CreateProductRequest productRequest) {
        ProductEntity productEntity = productMapper.createProductRequestToProductEntity(productRequest);
        productRepository.save(productEntity);
    }

    @Override
    public GetProductDetailsResponse retrieveProductDetailsById(long id) {
        final ProductEntity productEntity = productRepository.findById(id).orElseThrow(()
                -> new ProductNotFoundException(String.format("Product with id %d not found", id)));
        return groupedProductMapper.createGroupedProductEntityToGetProductDetailsResponse(productEntity.getGroupedProductsEntity());
    }

    @Override
    public void updateProduct(Long id, UpdateProductRequest updateProductRequest) {
         ProductEntity productEntity = productRepository.findById(id).orElseThrow(()
                -> new ProductNotFoundException(String.format("Product with id %d not found", id)));
         productMapper.updateProductEntityWithUpdateProductRequest(productEntity, updateProductRequest);
         productRepository.save(productEntity);
    }

    private GetProductResponse createGetProductResponse(long id) {
        final ProductEntity productEntity = productRepository.findById(id).orElseThrow(()
                -> new ProductNotFoundException(String.format("Product with id %d not found", id)));

        final MasterProduct masterProduct = productIntegration
                .retrieveMasterProductById(id)
                .block();
        final ProductCacheObject productCacheObject = getProductDataFromCache();
        return getGetProductResponse(productEntity, masterProduct, productCacheObject);
    }

    private GetProductResponse getGetProductResponse(ProductEntity productEntity, MasterProduct masterProduct, ProductCacheObject productCacheObject) {
        GetProductResponse getProductResponse = productMapper.productEntityToGetProductResponse(productEntity);
        getProductResponse.setStockQuantity(masterProduct.getStockQuantity());
        getProductResponse.setAvatar(masterProduct.getAvatar());
        getProductResponse.setSaleAverage(masterProduct.getSaleAverage());
        getProductResponse.setCreatedAt(masterProduct.getCreatedAt());
        getProductResponse.setCompany(productCacheObject.getCompany());
        getProductResponse.setAddress(productCacheObject.getAddress());
        return getProductResponse;
    }

    private ProductCacheObject getProductDataFromCache() {
        final ProductCacheObject productCacheObject;
        try {
            productCacheObject = productCache.getProductCache();
        } catch (ExecutionException e) {
            throw new ProductCacheException("An error occurred when retrieving data from cache provider");
        }
        return productCacheObject;
    }
}

package com.app.company.productapi.integration;

import com.app.company.productapi.integration.data.MasterProduct;
import reactor.core.publisher.Mono;

public interface MasterProductIntegration {
    Mono<MasterProduct> retrieveMasterProductById(long id);
}

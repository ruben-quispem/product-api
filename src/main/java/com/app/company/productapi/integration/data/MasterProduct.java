package com.app.company.productapi.integration.data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class MasterProduct {
    private long id;
    private int stockQuantity;
    private double saleAverage;
    private String avatar;
    private LocalDateTime createdAt;
}

package com.app.company.productapi.integration;

import com.app.company.productapi.integration.data.MasterProduct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service
public class MockApiMasterProductProvider implements MasterProductIntegration{

    private final String url;

    @Autowired
    public MockApiMasterProductProvider(@Value("${product.master.integration.url}") String url) {
        this.url = url;
    }

    @Override
    public Mono<MasterProduct> retrieveMasterProductById(long id) {
        return WebClient.create(url)
                .get()
                .uri("/master-products/" + id)
                .retrieve()
                .bodyToMono(MasterProduct.class);
    }
}

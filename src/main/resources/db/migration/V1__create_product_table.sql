-- CREATE SCHEMA IF NOT EXISTS 'product_db';
DROP TABLE IF EXISTS product;
CREATE TABLE product(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    alcohol bit NULL,
    is_active bit NULL,
    name varchar(255) NULL,
    statistic_flag bit NULL,
    type varchar(255) NULL
    );

DROP TABLE IF EXISTS grouped_product;
CREATE TABLE grouped_product(
    id int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    brand_name varchar(255) NULL,
    caliber_name varchar(255) NULL,
    family_name varchar(255) NULL,
    flavor_name varchar(255) NULL,
    is_recyclable bit NULL,
    product_id int NOT NULL,
    FOREIGN KEY (product_id)
        REFERENCES product(id)
    );

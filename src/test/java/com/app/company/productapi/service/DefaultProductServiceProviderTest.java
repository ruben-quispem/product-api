package com.app.company.productapi.service;

import com.app.company.productapi.cache.ProductCache;
import com.app.company.productapi.cache.ProductCacheObject;
import com.app.company.productapi.dto.CreateProductRequest;
import com.app.company.productapi.dto.GetProductDetailsResponse;
import com.app.company.productapi.dto.GetProductResponse;
import com.app.company.productapi.dto.UpdateProductRequest;
import com.app.company.productapi.entities.GroupedProductEntity;
import com.app.company.productapi.entities.ProductEntity;
import com.app.company.productapi.exceptions.ProductCacheException;
import com.app.company.productapi.exceptions.ProductNotFoundException;
import com.app.company.productapi.integration.MasterProductIntegration;
import com.app.company.productapi.integration.data.MasterProduct;
import com.app.company.productapi.mapper.GroupedProductMapper;
import com.app.company.productapi.mapper.ProductMapper;
import com.app.company.productapi.repository.ProductRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import reactor.core.publisher.Mono;

import java.util.Optional;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DefaultProductServiceProviderTest {

    @Mock
    private MasterProductIntegration productIntegration;
    @Mock
    private ProductRepository productRepository;
    @Mock
    private ProductMapper productMapper;
    @Mock
    private GroupedProductMapper groupedProductMapper;
    @Mock
    private ProductCache productCache;

    @InjectMocks
    private DefaultProductServiceProvider defaultProductServiceProvider;

    @Test
    void retrieveProductById() throws ExecutionException {
        long wantedProductId = 1001;
        ProductEntity productEntity = ProductEntity.builder().
                id(1l).name("Test product").type("Test type").build();
        when(productRepository.findById(wantedProductId)).thenReturn(Optional.of(productEntity));

        when(productIntegration.retrieveMasterProductById(wantedProductId))
                .thenReturn(Mono.just(MasterProduct.builder().id(1).saleAverage(222.10).stockQuantity(10).build()));

        when(productCache.getProductCache())
                .thenReturn(ProductCacheObject.builder().company("Test Company").address("Test Address").build());

        when(productMapper.productEntityToGetProductResponse(productEntity))
                .thenReturn(GetProductResponse.builder().productName(productEntity.getName()).productType(productEntity.getType()).build());

        final GetProductResponse getProductResponse = defaultProductServiceProvider.retrieveProductById(wantedProductId);

        assertNotNull(getProductResponse);
        assertEquals("Test product", getProductResponse.getProductName());
    }

    @Test
    void throwsProductNotFoundExceptionWhenProductIdDoesNotExist() {
        long nonExistingId = 1001;
        when(productRepository.findById(nonExistingId)).thenReturn(Optional.empty());
        assertThrows(ProductNotFoundException.class, () -> {
            defaultProductServiceProvider.retrieveProductById(nonExistingId);
        });
    }

    @Test
    void throwsProductCacheExceptionWhenAnErrorOccurredInCacheProvider() throws ExecutionException {
        long wantedProductId = 1001;
        ProductEntity productEntity = ProductEntity.builder().
                id(1l).name("Test product").type("Test type").build();
        when(productRepository.findById(wantedProductId)).thenReturn(Optional.of(productEntity));

        when(productIntegration.retrieveMasterProductById(wantedProductId))
                .thenReturn(Mono.just(MasterProduct.builder().id(1).saleAverage(222.10).stockQuantity(10).build()));

        when(productCache.getProductCache())
                .thenThrow(new ExecutionException("error in cache", null));

        assertThrows(ProductCacheException.class, () -> {
            defaultProductServiceProvider.retrieveProductById(wantedProductId);
        });
    }

    @Test
    void saveProduct() {
        CreateProductRequest createProductRequest = CreateProductRequest.builder().productName("Test product").productType("Test type").build();
        ProductEntity productEntity =  ProductEntity.builder().
                id(1l).name("Test product").type("Test type").build();
        when(productMapper.createProductRequestToProductEntity(createProductRequest)).thenReturn(productEntity);
        defaultProductServiceProvider.saveProduct(createProductRequest);
        verify(productRepository, times(1)).save(productEntity);

    }

    @Test
    void retrieveProductDetailsById() {
        long wantedProductId = 1001;
        ProductEntity productEntity = ProductEntity.builder().
                id(1l).name("Test product").type("Test type")
                .groupedProductsEntity(GroupedProductEntity.builder().id(10)
                        .brandName("Test Brand Name")
                        .caliberName("Test Caliber name")
                        .familyName("Test Family name")
                        .flavorName("Test flavor name").build())
                .build();

        GetProductDetailsResponse getProductDetailsResponse = GetProductDetailsResponse.builder()
                .productBrand(productEntity.getGroupedProductsEntity().getBrandName())
                .productCaliber(productEntity.getGroupedProductsEntity().getCaliberName())
                .productFamily(productEntity.getGroupedProductsEntity().getFamilyName())
                .productFlavor(productEntity.getGroupedProductsEntity().getFlavorName()).build();

        when(productRepository.findById(wantedProductId)).thenReturn(Optional.of(productEntity));
        when(groupedProductMapper.createGroupedProductEntityToGetProductDetailsResponse(productEntity.getGroupedProductsEntity()))
                .thenReturn(getProductDetailsResponse);

        final GetProductDetailsResponse getProductDetailsResponseResult = defaultProductServiceProvider.retrieveProductDetailsById(wantedProductId);
        assertNotNull(getProductDetailsResponseResult);
        assertEquals("Test Brand Name", getProductDetailsResponseResult.getProductBrand());
        assertEquals("Test Caliber name", getProductDetailsResponseResult.getProductCaliber());
    }

    @Test
    void throwsProductNotFoundExceptionWhenProductDetailsIdDoesNotExist() {
        long nonExistingId = 1001;
        when(productRepository.findById(nonExistingId)).thenReturn(Optional.empty());
        assertThrows(ProductNotFoundException.class, () -> {
            defaultProductServiceProvider.retrieveProductDetailsById(nonExistingId);
        });
    }

    @Test
    void updateProduct() {
        long wantedProductId = 1001;
        UpdateProductRequest updateProductRequest = UpdateProductRequest.builder().productName("Test product").productType("Test type").build();
        ProductEntity productEntity =  ProductEntity.builder().
                id(1l).name("Test product").type("Test type").build();

        when(productRepository.findById(wantedProductId)).thenReturn(Optional.of(productEntity));
        doNothing().when(productMapper).updateProductEntityWithUpdateProductRequest(productEntity, updateProductRequest);
        defaultProductServiceProvider.updateProduct(wantedProductId, updateProductRequest);
        verify(productRepository, times(1)).save(productEntity);
    }

    @Test
    void throwsProductNotFoundExceptionWhenUpdateProductWithIdDoesNotExist() {
        long nonExistingId = 1001;
        when(productRepository.findById(nonExistingId)).thenReturn(Optional.empty());
        assertThrows(ProductNotFoundException.class, () -> {
            defaultProductServiceProvider.updateProduct(nonExistingId, UpdateProductRequest.builder().build());
        });
    }
}

package com.app.company.productapi.resources;

import com.app.company.productapi.dto.CreateProductRequest;
import com.app.company.productapi.dto.GetProductDetailsResponse;
import com.app.company.productapi.dto.GetProductResponse;
import com.app.company.productapi.dto.UpdateProductRequest;
import com.app.company.productapi.exceptions.ProductCacheException;
import com.app.company.productapi.exceptions.ProductNotFoundException;
import com.app.company.productapi.service.ProductService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ProductApi.class)
class ProductApiTest {

    private final String baseURL = "http://localhost:8080/api/v1/products";

    @MockBean
    private ProductService productService;
    @Autowired
    private MockMvc mockMvc;

    @Test
    void saveProduct() throws Exception {
        CreateProductRequest objectiveResultsRequest = CreateProductRequest.builder()
                .productName("testProduct")
                .productType("test type")
                .statisticFlag(true)
                .alcohol(true)
                .build();

        String requestJson = new ObjectMapper().writeValueAsString(objectiveResultsRequest);
        this.mockMvc.perform(post(baseURL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isCreated());

        verify(productService, times(1)).saveProduct(ArgumentMatchers.refEq(objectiveResultsRequest));
    }

    @Test
    void responseBadRequestWhenProductNameNotProvidedInSaveProductService() throws Exception {
        CreateProductRequest objectiveResultsRequest = CreateProductRequest.builder()
                .productType("test type")
                .statisticFlag(true)
                .alcohol(true)
                .build();

        String requestJson = new ObjectMapper().writeValueAsString(objectiveResultsRequest);
        this.mockMvc.perform(post(baseURL)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isBadRequest());
    }

    @Test
    void updateProduct() throws Exception {
        UpdateProductRequest objectiveResultsRequest = UpdateProductRequest.builder()
                .productName("testProduct")
                .productType("test type")
                .statisticFlag(true)
                .alcohol(true)
                .build();

        String requestJson = new ObjectMapper().writeValueAsString(objectiveResultsRequest);
        this.mockMvc.perform(put(baseURL + "/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestJson))
                .andExpect(status().isOk());

        verify(productService, times(1)).updateProduct(ArgumentMatchers.refEq(1l),ArgumentMatchers.refEq(objectiveResultsRequest));
    }

    @Test
    void retrieveProductById() throws Exception {
        GetProductResponse getProductResponse = GetProductResponse.builder()
                .company("Product ABC")
                .address("2129 Old Dear Lane")
                .productName("testProduct")
                .productType("test type")
                .statisticFlag(true)
                .alcohol(true)
                .avatar("https://cdn.fakercloud.com/avatars/carlosgavina_128.jpg")
                .stockQuantity(10)
                .createdAt(LocalDateTime.now())
                .build();
        when(productService.retrieveProductById(1l)).thenReturn(getProductResponse);

        this.mockMvc.perform(get(baseURL + "/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.company").value("Product ABC"))
                .andExpect(jsonPath("$.address").value("2129 Old Dear Lane"))
                .andExpect(jsonPath("$.productName").value("testProduct"))
                .andExpect(jsonPath("$.productType").value("test type"))
                .andExpect(jsonPath("$.statisticFlag").value(true))
                .andExpect(jsonPath("$.alcohol").value(true))
                .andExpect(jsonPath("$.avatar").value("https://cdn.fakercloud.com/avatars/carlosgavina_128.jpg"))
                .andExpect(jsonPath("$.stockQuantity").value(10));
    }

    @Test
    void responseNotFoundWhenProductIdDoesNotExist() throws Exception {
        when(productService.retrieveProductById(1l)).thenThrow(new ProductNotFoundException("Product not found"));
        this.mockMvc.perform(get(baseURL + "/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    void responseInternalServerErrorWhenProductCacheProviderThrowException() throws Exception {
        when(productService.retrieveProductById(1l)).thenThrow(new ProductCacheException("An error occurred in cache provider"));
        this.mockMvc.perform(get(baseURL + "/1"))
                .andExpect(status().is5xxServerError());
    }

    @Test
    void retrieveProductDetailsById() throws Exception {
        GetProductDetailsResponse getProductResponse = GetProductDetailsResponse.builder()
                .productBrand("test brand")
                .productCaliber("test caliber")
                .productFamily("test family")
                .productFlavor("test flavor")
                .recyclable(true)
                .build();
        when(productService.retrieveProductDetailsById(1l)).thenReturn(getProductResponse);

        this.mockMvc.perform(get(baseURL + "/1/details")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.productBrand").value("test brand"))
                .andExpect(jsonPath("$.productCaliber").value("test caliber"))
                .andExpect(jsonPath("$.productFamily").value("test family"))
                .andExpect(jsonPath("$.productFlavor").value("test flavor"))
                .andExpect(jsonPath("$.recyclable").value(true));
    }
}

# Product API Service
It is a simple app that expose few endpoints to manage products.

### Installation
customSearchEngineResults requires:
- [JDK.11](https://openjdk.java.net/projects/jdk/11/) + to run.
- IDE with lombok plugin for [Eclipse](https://projectlombok.org/setup/eclipse) or [Intellij idea](https://plugins.jetbrains.com/plugin/6317-lombok)
- [Maven](https://maven.apache.org/download.cgi)
- [Docker](https://www.docker.com/) (If want to deploy the app in a docker container)

Clone the repo and run the app
```sh
$ cd your_dev_directory 
$ git clone https://gitlab.com/ruben-quispem/product-api.git
$ mvn clean package
$ mvn spring-boot:run
```

Or install and run it
```sh
$ mvn clean install
$ java -jar your_project_path/target/product-api-0.0.1-SNAPSHOT.jar
```
In order to access product api response time logs, there is a folder **logs** in the project root directory
with a file **api-response-time.log**

It will launch the product api service with an in memory database.
To access h2 database
```sh
http://localhost:8080/h2
userName: sa
password: password
```

### Docker
Product API service is very easy to install and deploy in a Docker container.

When ready, simply use the Dockerfile to build the image.

```sh
cd product-api
docker build -t rquispe/product-api:0.0.1-SNAPSHOT .
```
This will create the search-engine image and pull in the necessary dependencies.

Once done, run the Docker image:

```sh
docker run --name product-api  -p 8080:8080 rquispe/product-api:0.0.1-SNAPSHOT

docker exec -it product-api bash
cd home/app/logs
cat api-response-time.log
```

### Docker Compose
In order to launch the product service api with a mysql database
```sh
docker-compose up -d
```

In order to check api logs
```sh
docker-compose logs -f product-api-service
```
In order to check api logs with response time, go inside docker container
```sh
docker exec -it product-api-service bash
cd home/app/logs
cat api-response-time.log
```
### SWAGGER

In order to access swagger ui go to:
- [Swagger UI](http://localhost:8080/swagger-ui/index.html)

### POSTMAN
- In order to test the product service API using postman, check the **Product-API.postman_collection.json** file

### Health Check
- [Health check service](http://localhost:8080/actuator/health/custom)

### APIs used
- [Mock API IO](https://mockapi.io/)

### Tools used
- [mapstruct](https://mapstruct.org/)
- [lombok](https://projectlombok.org/)

### Todos
- Write MORE Tests
- Add CI/CD pipeline

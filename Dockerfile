FROM openjdk:11
EXPOSE 8080
ADD target/*.jar app.jar
ENV APP_LOG_DIRECTORY=/home/app/logs
VOLUME $APP_LOG-DIRECTORY
ENTRYPOINT ["java", "-jar", "/app.jar", "-web -webAllowOthers -tcp -tcpAllowOthers -browser"]
